import { CssBaseline } from '@material-ui/core';
import React, { Component } from 'react';
import AllOffPistes from './pages/AllOffPistes';

class App extends Component {
    render() {
        return (
            <div className="App">
                <CssBaseline/>
                <AllOffPistes/>
            </div>
        );
    }
}

export default App;
