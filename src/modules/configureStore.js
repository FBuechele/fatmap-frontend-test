import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from './rootReducer';
import thunkMiddleware from 'redux-thunk';

// in a real project I would make separate configureStore files for development and production environments
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export function configureStore() {
    return createStore(
        rootReducer,
        composeEnhancers(
            applyMiddleware(thunkMiddleware),
        ),
    );
}

