import update from 'immutability-helper';

const actionTypes = {
    LOAD_OFF_PISTE_DATA_REQUEST: "LOAD_OFF_PISTE_DATA_REQUEST",
    LOAD_OFF_PISTE_DATA_SUCCESS: "LOAD_OFF_PISTE_DATA_SUCCESS",
    LOAD_OFF_PISTE_DATA_ERROR: "LOAD_OFF_PISTE_DATA_ERROR",
    SET_OFF_PISTE_DATA_SORT_BY: "SET_OFF_PISTE_DATA_SORT_BY",
};

const defaultState = {
    isLoading: false,
    error: null,
    result: null,
    sortBy: 'name'
};

export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case actionTypes.LOAD_OFF_PISTE_DATA_REQUEST: {
            return update(state, {
                isLoading: { $set: true },
                result: { $set: null },
                error: { $set: null }
            });
        }
        case actionTypes.LOAD_OFF_PISTE_DATA_SUCCESS: {
            return update(state, {
                isLoading: { $set: false },
                result: { $set: sortOffPisteData(action.data, state.sortBy) },
                error: { $set: null }
            });
        }
        case actionTypes.LOAD_OFF_PISTE_DATA_ERROR: {
            return update(state, {
                isLoading: { $set: false },
                result: { $set: null },
                error: { $set: action.error }
            });
        }
        case actionTypes.SET_OFF_PISTE_DATA_SORT_BY: {
            return update(state, {
                sortBy: { $set: action.data },
                result: { $set: sortOffPisteData(state.result, action.data) }
            });
        }
        default:
            return state;
    }
}

export function loadOffPisteData() {
    return function (dispatch) {
        dispatch({ type: actionTypes.LOAD_OFF_PISTE_DATA_REQUEST });
        fetch("/data/off-pistes.json")
            .then(response => response.json())
            .then(function (data) {
                dispatch({ type: actionTypes.LOAD_OFF_PISTE_DATA_SUCCESS, data: data });
            }).catch(e => dispatch({ type: actionTypes.LOAD_OFF_PISTE_DATA_ERROR, error: e.toString() }));
    };
}

export function setOffPisteDataSortBy(sortBy) {
    return { type: actionTypes.SET_OFF_PISTE_DATA_SORT_BY, data: sortBy };
}

function sortOffPisteData(elements, sortBy) {
    if (elements) {
        return elements.sort((a, b) => {
            if (a.hasOwnProperty(sortBy) && b.hasOwnProperty(sortBy)) {
                if (a[sortBy] < b[sortBy]) {
                    return -1;
                }
                if (a[sortBy] > b[sortBy]) {
                    return 1;
                }
            }
        });
    } else {
        return 0;
    }
}