import { combineReducers } from 'redux';
import offPistes from './offPistesModule';

const rootReducer = combineReducers({
    offPistes
});

export default rootReducer;
