import Container from '@material-ui/core/Container/Container';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import React from 'react';
import OffPisteOverviewContainer from '../containers/offPistes/OffPisteOverviewContainer';

const styles = theme => ({
    root: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
    }
});

function AllOffPistes(props) {
    return (
        <Container maxWidth="md" className={props.classes.root}>
            <OffPisteOverviewContainer/>
        </Container>
    );
}

AllOffPistes.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AllOffPistes);