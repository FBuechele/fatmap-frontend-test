import { CircularProgress, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import OffPisteTeaser from '../../components/offPistes/OffPisteTeaser';
import { loadOffPisteData, setOffPisteDataSortBy } from '../../modules/offPistesModule';

const sortingPropertyOptions = ['name', 'ski_difficulty'];

class OffPisteOverviewContainer extends React.Component {
    componentDidMount() {
        this.props.loadOffPistes();
    }

    initialDataLoaded() {
        return this.props.offPistes && !this.props.isLoading;
    }

    hasError() {
        return this.props.error;
    }

    renderErrorContent() {
        return <span>{this.props.error}</span>
    }

    renderLoadingContent() {
        return <CircularProgress/>;
    }

    renderContent() {
        const { offPistes, sortBy } = this.props;

        const handleSortPropertyChange = (event) => {
            this.props.setSortBy(event.target.value)
        };
        return (
            <div>
                <FormControl>
                    <InputLabel htmlFor="sort-by">Sort by</InputLabel>
                    <Select
                        value={sortBy}
                        onChange={handleSortPropertyChange}
                        inputProps={{
                            name: 'sortBY',
                            id: 'sort-by',
                        }}
                    >
                        {sortingPropertyOptions.map(option => (
                            <MenuItem key={option} value={option}>{option}</MenuItem>))}
                    </Select>
                </FormControl>
                <div>
                    {offPistes.map((offPiste) => <OffPisteTeaser key={offPiste.id} offPiste={offPiste}/>)}
                </div>
            </div>
        );
    }

    render() {
        if (this.hasError()) {
            return this.renderErrorContent();
        }
        if (this.initialDataLoaded()) {
            return this.renderContent();
        }
        return this.renderLoadingContent();
    }
}

OffPisteOverviewContainer.propTypes = {
    sortBy: PropTypes.string.isRequired,
    loadOffPistes: PropTypes.func.isRequired,
    setSortBy: PropTypes.func.isRequired,
    offPistes: PropTypes.array,
    error: PropTypes.string,
};

const mapStateToProps = state => ({
    offPistes: state.offPistes.result,
    isLoading: state.offPistes.isLoading,
    error: state.offPistes.error,
    sortBy: state.offPistes.sortBy
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadOffPistes: loadOffPisteData,
    setSortBy: setOffPisteDataSortBy
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OffPisteOverviewContainer);