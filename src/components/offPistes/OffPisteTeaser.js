import { Paper, Typography, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';

const styles = theme => ({
    root: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        padding: theme.spacing(2)
    }
});

function OffPisteTeaser(props) {
    const { classes } = props;
    return (
        <Paper className={classes.root}>
            <Typography variant="h5">{props.offPiste.name}</Typography>
            <Typography variant="body1">{props.offPiste.short_description}</Typography>
            <Typography variant="body1">Ski difficulty: {props.offPiste.ski_difficulty}</Typography>
        </Paper>
    );
}

OffPisteTeaser.propTypes = {
    classes: PropTypes.object.isRequired,
    offPiste: PropTypes.object.isRequired
};

export default withStyles(styles)(OffPisteTeaser);