# FATMAP Frontend Developer Task

### Install and start for development

```
yarn install
```
```
yarn start
```


### Notes
As this was only a small example I didn't do all the setup I would usually do. For example, I would write a 
standardized procedure for async data loading in redux which can be reused for every request. 
I am also a supporter of TypeScript (or similar) but I didn't use it for this example, since I am not completely fluent
with it in the React context and didn't want to exceed the time limit too much or use it sloppily. 
The creation of the general structure took the most time for me, therefore I didn't do the bonus task anymore.

I am looking forward to a discussion and your feedback.